﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SchoolClasses.Models;

namespace SchoolClasses.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Classes> Classes { get; set; }
        public DbSet<Students> Students { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Students>()
                .HasOne(p => p.Class)
                .WithMany(b => b.Students);
        }
    }
}
