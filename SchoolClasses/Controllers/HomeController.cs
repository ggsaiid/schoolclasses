﻿using Microsoft.AspNetCore.Mvc;
using SchoolClasses.Contracts;
using SchoolClasses.LogProvider;
using SchoolClasses.Models;

namespace SchoolClasses.Controllers
{
    public class HomeController : Controller
    {
        private readonly IClassesService _service;

        public HomeController(IClassesService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult List()
        {
            return Json(_service.GetClassesList());
        }

        [HttpPost]
        public IActionResult Create(Classes classInfo)
        {
            if (ModelState.IsValid)
            {
                if (_service.Create(classInfo) > 0)
                {
                    return Json(Message.Success("New Class added."));
                }
                else
                {
                    return Json(Message.Warning("Failed"));
                }
            }
            _service.LogWarning(LoggingEvents.INSERT_ITEM, "Class Information is not Valid.");
            return Json(Message.Warning("Class Information is not valid."));
        }

        [HttpPost]
        public IActionResult Update(Classes classInfo)
        {
            if (ModelState.IsValid)
            {
                if (_service.Update(classInfo) > 0)
                {
                    return Json(Message.Success("the Class Information Updated."));
                }
                else
                {
                    return Json(Message.Warning("the Class Updating Failed."));
                }
            }
            _service.LogWarning(LoggingEvents.UPDATE_ITEM, "Class Information is not Valid.");
            return Json(Message.Warning("Class Information is not valid."));
        }

        [HttpPost]
        public IActionResult Delete(int classId)
        {
            if (_service.Delete(classId) > 0)
            {
                return Json(Message.Success("the Class Deleted."));
            }
            else
            {
                return Json(Message.Warning("Failed"));
            }
        }

        [HttpPost]
        public IActionResult Select(int classId)
        {
            var classInfo = _service.Select(classId);
            if (classInfo == null)
            {
                return Json(Message.Fail("the Class Not Found."));
            }
            else
            {
                return Json(classInfo);
            }
        }
    }
}