﻿using Microsoft.AspNetCore.Mvc;
using SchoolClasses.Contracts;
using SchoolClasses.LogProvider;
using SchoolClasses.Models;

namespace SchoolClasses.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IStudentsService _service;

        public StudentsController(IStudentsService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult List(int classId)
        {
            return Json(_service.GetStudentsList(classId));
        }

        [HttpPost]
        public IActionResult Create(Students studentInfo)
        {
            if (ModelState.IsValid)
            {
                int res = _service.Create(studentInfo);
                if (res > 0)
                {
                    return Json(Message.Success("New Student added."));
                }
                else if (res == -10)
                {
                    return Json(Message.Warning("Student Information is already Exist."));
                }
                else
                {
                    return Json(Message.Warning("Failed"));
                }
            }
            _service.LogWarning(LoggingEvents.INSERT_ITEM, "Student Information is not Valid.");
            return Json(Message.Warning("Student Information is not valid."));
        }

        [HttpPost]
        public IActionResult Update(Students studentInfo)
        {
            if (ModelState.IsValid)
            {
                int res = _service.Update(studentInfo);
                if (res > 0)
                {
                    return Json(Message.Success("the Student Information Updated."));
                }
                else if (res == -10)
                {
                    return Json(Message.Warning("Student Information is already Exist."));
                }
                else
                {
                    return Json(Message.Warning("the Student Updating Failed."));
                }
            }
            _service.LogWarning(LoggingEvents.UPDATE_ITEM, "Student Information is not Valid.");
            return Json(Message.Warning("Student Information is not valid."));
        }

        [HttpPost]
        public IActionResult Delete(int studentId)
        {
            if (_service.Delete(studentId) > 0)
            {
                return Json(Message.Success("the Student Deleted."));
            }
            else
            {
                return Json(Message.Warning("Failed"));
            }
        }

        [HttpPost]
        public IActionResult Select(int studentId)
        {
            var studentInfo = _service.Select(studentId);
            if (studentInfo == null)
            {
                return Json(Message.Fail("the Student Not Found."));
            }
            else
            {
                return Json(studentInfo);
            }
        }
    }
}