﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SchoolClasses.Models
{
    public class Classes
    {
        [Key]
        [Display(Description = "Id")]
        public int ClassId { get; set; }

        [Required]
        [Display(Description = "Class Name")]     
        public string ClassName { get; set; }

        [Required]
        [Display(Description = "Location")]
        public string Location { get; set; }

        [Required]
        [Display(Description = "Teacher Name")]
        public string TeacherName { get; set; }

        public List<Students> Students { get; set; }
    }
}
