﻿using System.ComponentModel.DataAnnotations;

namespace SchoolClasses.Models
{
    public class Students
    {
        [Key]
        [Display(Description = "Id")]
        public int StudentId { get; set; }

        [Required]
        [Display(Description = "Student Name")]
        public string StudentName { get; set; }

        [Required]
        [Display(Description = "Age")]
        public string Age { get; set; }

        [Required]
        [Display(Description = "GPA")]
        public string GPA { get; set; }

        [Required]
        public int ClassId { get; set; }
        public Classes Class { get; set; }
    }
}
