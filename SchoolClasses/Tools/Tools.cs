﻿public static class Message
{
    public static object Success(string Message)
    {
        return new { actionstate = "success", actionmessage = Message };
    }
    public static object Warning(string Message)
    {
        return new { actionstate = "warning", actionmessage = Message };
    }
    public static object Fail(string Message)
    {
        return new { actionstate = "danger", actionmessage = Message };
    }
}