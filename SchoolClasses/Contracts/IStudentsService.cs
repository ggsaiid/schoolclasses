﻿using SchoolClasses.LogProvider;
using SchoolClasses.Models;
using System.Collections.Generic;

namespace SchoolClasses.Contracts
{
    public interface IStudentsService
    {
        int Create(Students studentInfo);
        int Update(Students studentInfo);
        int Delete(int studentId);

        List<Students> GetStudentsList(int classId);
        Students Select(int studentId);

        bool Exists(int studentId);

        void LogWarning(LoggingEvents loggingEvents, string message);
    }
}