﻿using SchoolClasses.LogProvider;
using SchoolClasses.Models;
using System.Collections.Generic;

namespace SchoolClasses.Contracts
{
    public interface IClassesService
    {
        int Create(Classes classInfo);
        int Update(Classes classInfo);
        int Delete(int classId);

        List<Classes> GetClassesList();
        Classes Select(int classId);

        bool Exists(int classId);

        void LogWarning(LoggingEvents loggingEvents, string message);
    }
}