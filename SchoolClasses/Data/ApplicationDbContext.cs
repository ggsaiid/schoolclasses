﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SchoolClasses.Models;

namespace SchoolClasses.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<EventLog> EventLog { get; set; }
        //
        public DbSet<Classes> Classes { get; set; }
        public DbSet<Students> Students { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EventLog>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");
                entity.Property(e => e.EventId).HasColumnName("EventID");
                entity.Property(e => e.LogLevel).HasMaxLength(50);
                entity.Property(e => e.Message).HasMaxLength(4000);
            });

            modelBuilder.Entity<Students>()
                .HasOne(p => p.Class)
                .WithMany(b => b.Students);
        }
    }
}
