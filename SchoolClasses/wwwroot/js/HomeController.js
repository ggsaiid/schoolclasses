﻿var Class = {
    SelectedClassID: "",
    LoadClassList: function () {
        $.ajax({
            type: 'POST',
            url: "/Home/List/",
            data: { Nothing: "Nothing" },
            success: function (Res) {
                var TableID = "ClassListTable";
                Table.Initial(TableID, true);

                Table.AddColumn(TableID, "classId", "Class ID", "{classId}", false, false);
                Table.AddColumn(TableID, "classname", "Class Name", "{className}", false, false);
                Table.AddColumn(TableID, "location", "Location", "{location}", false, false);
                Table.AddColumn(TableID, "teachername", "Teacher Name", "{teacherName}", false, false);

                Table.AddColumn(TableID, "Students", "Students", '<a onclick="Student.ShowStudents(\'{classId}\')" class="btn btn-primary">Students</a>', false);

                Table.AddColumn(TableID, "Edit", "Edit", '<a onclick="Class.Select(\'{classId}\')" class="btn btn-warning">Select</a>', false);

                Table.AddColumn(TableID, "Delete", "Delete", '<a onclick="Class.Delete(\'{classId}\')" class="btn btn-danger">Delete</a>', false);

                Table.Create(TableID, Res, Res, Class.LoadClassList, false);
            }
        });
    },
    ShowClassInfo: function () {
        $("#modal-classinfo").modal('show');
    },
    ClearClassInfo: function () {
        $('#modal-classinfo').find('input:text').val('');
    },
    Insert: function () {
        $.ajax({
            type: 'POST',
            url: "/Home/Create/",
            data: $('#ClassInfo').serialize(),
            dataType: "json",
            success: function (Res) {
                $("#modal-classinfo").modal('hide');
                Class.LoadClassList();    
                Class.ClearClassInfo();
                $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
            }
        });
    },
    Delete: function (id) {
        $.ajax({
            type: 'POST',
            url: "/Home/Delete/",
            data: { classId: id },
            success: function (Res) {
                Class.LoadClassList();
                $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
            }
        });
    },
    Select: function (id) {
        Class.SelectedClassID = id;
        $.ajax({
            type: 'POST',
            url: "/Home/Select/",
            data: { classId: id },
            success: function (Res) {
                Class.ClearClassInfo();
                Class.ShowClassInfo();
                if (Res["actionstate"] == undefined) {
                    DataBind(Res);
                }
                else {
                    $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
                }
            }
        });
    },
    Update: function () {
        $.ajax({
            type: 'POST',
            url: "/Home/Update/",
            data: "classId=" + Class.SelectedClassID + "&" +
                $('#ClassInfo').serialize(),
            dataType: "json",
            success: function (Res) {
                $("#modal-classinfo").modal('hide');
                Class.LoadClassList();   
                Class.ClearClassInfo();
                Class.SelectedClassID = "";
                $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
            }
        });
    }
}
$(document).ready(function () {
    Class.LoadClassList(); 
});