﻿var Student = {
    SelectedClassID: "",
    SelectedStudentID: "",
    ShowStudents: function (Id) {
        $("#DIV_StudentList").hide(100);
        $("#DIV_StudentList").show(300);
        Student.LoadStudentList(Id);
    },
    LoadStudentList: function (Id) {
        Student.SelectedClassID = Id;
        $.ajax({
            type: 'POST',
            url: "/Students/List/",
            data: { classId: Id },
            dataType: "json",
            success: function (Res) {
                var TableID = "StudentListTable";
                Table.Initial(TableID, true);
                Table.AddColumn(TableID, "studentId", "Student ID", "{studentId}", false, false);
                Table.AddColumn(TableID, "studentName", "Student Name", "{studentName}", false, false);
                Table.AddColumn(TableID, "age", "Age", "{age}", false, false);
                Table.AddColumn(TableID, "GPA", "GPA", "{gpa}", false, false);

                Table.AddColumn(TableID, "Edit", "Edit", '<a onclick="Student.Select(\'{studentId}\')" class="btn btn-warning">Select</a>', false);

                Table.AddColumn(TableID, "Delete", "Delete", '<a onclick="Student.Delete(\'{studentId}\')" class="btn btn-danger">Delete</a>', false);

                Table.Create(TableID, Res, Res, Student.LoadStudentList, false);
            }
        });
    },
    ShowStudentInfo: function () {
        $("#modal-studentinfo").modal('show');
    },
    ClearStudentInfo: function () {
        $('#StudentInfo').find('input:text').val('');
    },
    Insert: function () {
        $.ajax({
            type: 'POST',
            url: "/Students/Create/",
            data: "classId=" + Student.SelectedClassID + "&" + $('#StudentInfo').serialize(),
            dataType: "json",
            success: function (Res) {
                $("#modal-studentinfo").modal('hide');
                Student.LoadStudentList(Student.SelectedClassID);
                Student.ClearStudentInfo();
                $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
            }
        });
    },
    Delete: function (id) {
        $.ajax({
            type: 'POST',
            url: "/Students/Delete/",
            data: { studentId: id },
            success: function (Res) {
                Student.LoadStudentList(Student.SelectedClassID);
                $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
            }
        });
    },
    Select: function (id) {
        Student.SelectedStudentID = id;
        $.ajax({
            type: 'POST',
            url: "/Students/Select/",
            data: { studentId: id },
            success: function (Res) {
                Student.ClearStudentInfo();
                Student.ShowStudentInfo();
                if (Res["actionstate"] == undefined) {
                    DataBind(Res);
                }
                else {
                    $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
                }
            }
        });
    },
    Update: function () {
        $.ajax({
            type: 'POST',
            url: "/Students/Update/",
            data: "studentId=" + Student.SelectedStudentID +
                "&classId=" + Student.SelectedClassID + "&" +
                $('#StudentInfo').serialize(),
            dataType: "json",
            success: function (Res) {
                $("#modal-studentinfo").modal('hide');
                Student.LoadStudentList(Student.SelectedClassID);
                Student.SelectedStudentID = "";
                Student.ClearStudentInfo();
                $.notify({ message: Res["actionmessage"] }, { type: Res["actionstate"] });
            }
        });
    }
} 