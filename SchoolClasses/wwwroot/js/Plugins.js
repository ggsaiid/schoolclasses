﻿function DataBind(Data) {
    if (Data != undefined && Data != null) {
        try {
            if (Data.constructor !== {}.constructor) {
                try { Data = JSON.parse(Data); if (Data.length == 1) { Data = Data[0]; } } catch (e) { return false; }
            }
            for (Key in Data) {
                var DataValue = Data[Key];
                var Item = $("#" + Key);
                if ((Item != null) && (Item != undefined)) { Item.val(DataValue); }
            }
            return true;
        }
        catch (e) { return false; }
    }
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
        "use strict";
        if (this === null) { throw new TypeError(); } var t = Object(this); var len = t.length >>> 0; if (len === 0) { return -1; } var n = 0; if (arguments.length > 1) { n = Number(arguments[1]); if (n != n) { n = 0; } else if (n !== 0 && n != Infinity && n != -Infinity) { n = (n > 0 || -1) * Math.floor(Math.abs(n)); } } if (n >= len) { return -1; } var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); for (; k < len; k++) { if (k in t && t[k] === searchElement) { return k; } } return -1;
    };
}
if (!String.prototype.startsWith) { String.prototype.startsWith = function (str) { return this.lastIndexOf(str, 0) === 0; }; }
if (!String.prototype.trim) { String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); }; }
if (!String.prototype.endsWith) { String.prototype.endsWith = function (searchString, position) { var subjectString = this.toString(); if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) { position = subjectString.length; } position -= searchString.length; var lastIndex = subjectString.indexOf(searchString, position); return lastIndex !== -1 && lastIndex === position; }; }
if (!String.prototype.formatEval) {
    String.prototype.formatEval = function () {
        var args = arguments[0];
        var Str = this;
        for (var i = 0; i < args.length; i++) {
            var _Value = args[i];
            var Vl = ""; try { Vl = eval(Str.split("{" + i + "}").join(args[i])) } catch (e) { Vl = Str.split("{" + i + "}").join(args[i]); }
            Str = Vl;
        }
        return Str;
    };
}
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments[0];
        var Str = this;
        for (var i = 0; i < args.length; i++) {
            Str = Str.split("{" + i + "}").join(args[i]);
        }
        return Str;
    };
}
if (!String.prototype.trimc) {
    String.prototype.trimc = function () {
        var Str = this;
        var charToRemove = arguments[0];
        while (Str.charAt(0) == charToRemove) {
            Str = Str.substring(1);
        }
        while (Str.charAt(Str.length - 1) == charToRemove) {
            Str = Str.substring(0, Str.length - 1);
        }
        return Str;
    };
}
Function.prototype._Name = function () { var result = /^function\s+([\w\$]+)\s*\(/.exec(this.toString()); return result ? result[1] : new Date().getMilliseconds; }
function CopyObject(OldObj) {
    if (null == OldObj || "object" != typeof OldObj)
        return OldObj;
    var NewObj = OldObj.constructor();
    for (var attr in OldObj) {
        if (OldObj.hasOwnProperty(attr))
            NewObj[attr] = OldObj[attr];
    }
    return NewObj;
}

var Table = {
    List: {},
    //
    TableStruct: { ID: "", PageIndex: 1, PageCount: 0, RowsCount: 9, Header: [], ReverseColumn: false, LoadFunction: null, LastPageIndex: 1 },
    TableStructClear: function () {
        this.TableStruct.ID = "";
        this.TableStruct.PageIndex = 1;
        this.TableStruct.PageCount = 0;
        this.TableStruct.RowsCount = 9;
        this.TableStruct.Header = [];
        this.TableStruct.ReverseColumn = false;
        this.TableStruct.LoadFunction = null;
        this.TableStruct.LastPageIndex = 1;
    },
    ColumnStruct: { ColumnName: "", Title: "", Value: "", Sortable: false, EvalMode: true },
    ColumnStructClear: function () {
        this.ColumnStruct.ColumnName = "";
        this.ColumnStruct.Title = "";
        this.ColumnStruct.Value = "";
        this.ColumnStruct.Sortable = false;
        this.ColumnStruct.EvalMode = true;
    },
    //
    ClearColumns: function (TableName) { this.List[TableName].Header = []; },
    ClearTable: function (TableName) { this.List[TableName] = CopyObject(this.ColumnStruct); document.getElementById(TableName).innerHTML = ""; },
    //
    Initial: function (TableName, ReverseColumn, PageSize) {
        if (this.List[TableName] == undefined || this.List[TableName] == null) {

            this.TableStructClear();
            this.ColumnStructClear();

            this.List[TableName] = CopyObject(this.TableStruct);
            this.List[TableName].ID = TableName;
            this.List[TableName].ReverseColumn = ReverseColumn;
            this.List[TableName].PageSize = (PageSize == undefined) ? 9 : PageSize;
        }
        else {
            this.List[TableName].ReverseColumn = ReverseColumn;
            this.List[TableName].Header = [];
        }
        return this;
    },
    AddColumn: function (TableName, ColumnName, Title, Value, Sortable, EvalMode) {
        var _Column = CopyObject(this.ColumnStruct);
        _Column.ColumnName = ColumnName;
        _Column.Title = Title;
        _Column.Value = Value;
        _Column.Sortable = Sortable;
        if (EvalMode != undefined) _Column.EvalMode = EvalMode;
        this.List[TableName].Header.push(_Column);
        return this;
    },
    //
    Create: function (TableName, JsonData, PageCount, FunctionSelf, AutoCallMode) {
        var PageCount = 0
        try { document.getElementById(TableName + "_MTable").remove(); }
        catch (e) { }
        var _Table = document.createElement("Table");
        _Table.id = TableName + "_MTable";
        _Table.className = "MTable MTableLtr";
        _Table.innerHTML = "<thead id=\"" + TableName + "_THead" + "\" ></thead>" + "<tbody id=\"" + TableName + "_TBody" + "\" ></tbody>";
        document.getElementById(TableName).appendChild(_Table);
        document.getElementById(TableName + "_THead").innerHTML = this.CreateHeader(TableName);

        this.List[TableName].PageCount = PageCount;

        document.getElementById(TableName + "_TBody").innerHTML = this.CreateRows(TableName, JsonData);
        if (!AutoCallMode) {
            try { document.getElementById(TableName + "_TFoot").remove(); } catch (e) { }
            var Pager = this.CreateFooter(TableName, JsonData);
            var node = document.createElement("tfoot");
            node.innerHTML = Pager;
            node.id = TableName + "_TFoot";
            document.getElementById(TableName + "_MTable").appendChild(node);
        }
        this.List[TableName].LoadFunction = FunctionSelf;
        this.ClearColumns(TableName);
    },
    CreateHeader: function (TableName) {
        try {
            var Header = "";
            if (this.List[TableName] != undefined && this.List[TableName] != null) {
                if (this.List[TableName].Header.length > 0) {
                    Header = "<tr>\r\n";
                    for (var i = 0; i < this.List[TableName].Header.length; i++) {
                        Header += "<th>" + this.List[TableName].Header[i].Title + "</th>";
                    }
                    Header += "</tr>\r\n";
                }
                return Header;
            }
        } catch (e) { }
    },
    CreateRows: function (TableName, JsonData) {
        try {
            if (JsonData != undefined || JsonData != null) {
                var Body = "";
                if (JsonData.constructor !== {}.constructor) {
                    if (JsonData.constructor !== [].constructor) {
                        try { JsonData = JSON.parse(JsonData); } catch (e) {
                            if (JsonData.constructor !== {}.constructor) {
                                alert("the data is not parsable"); return;
                            }
                        }
                    }
                }
                if (this.List[TableName] != undefined && this.List[TableName] != null) {
                    if (this.List[TableName].Header.length > 0) {
                        for (var i = 0; i < JsonData.length; i++) {
                            Body += "\r\n<tr>\r\n";
                            for (var j = 0; j < this.List[TableName].Header.length; j++) {
                                var _Template = this.List[TableName].Header[j].Value;
                                for (Key in JsonData[i]) { if (JsonData[i].hasOwnProperty(Key)) { _Template = _Template.split("{" + Key + "}").join(JsonData[i][Key]); } }
                                var _CellContent = "";
                                try {
                                    if (this.List[TableName].Header[j].EvalMode) {
                                        //
                                        _Template = _Template.trim().replace(/\r/g, "").replace(/\n/g, "");

                                        if ((_Template.startsWith("{{")) && (_Template.endsWith("}}"))) {
                                            _Template = _Template.trimc("{{").trimc("}}");
                                            _Template = "(function () {" + _Template + "}())";
                                        }
                                        // 
                                        _CellContent = eval(_Template);
                                    }
                                    else
                                        _CellContent = _Template;
                                } catch (e) { _CellContent = _Template; }
                                Body += "<td>" + _CellContent + "</td>";
                            }
                            Body += "</tr>\r\n";
                        }
                        if (JsonData.length <= 0) { Body += "<tr><td colspan=\"100%\">No Record</td></tr>"; }
                        if (JsonData.length == undefined) { Body += "<tr><td colspan=\"100%\">No Record</td></tr>"; }
                    }
                    return Body;
                }
            }
            else { return "<tr><td colspan=\"100%\">No Record</td></tr>"; }
        }
        catch (e) { }
    },
    CreateFooter: function (TableName, JsonData) {
        try {
            if (JsonData != undefined || JsonData != null) {
                try { JsonData = JSON.parse(JsonData); } catch (e) { }
                var TFoot = "";
                var a = "<tr><td colspan='100%'>" + TFoot + "<div class='" + TableName + "'></div></td></tr>";
                return a;
            }
            else { return "<tr><td colspan='100%'><div class='" + TableName + "'></div></td></tr>"; }
        } catch (e) { }
    },
    //
    ChangePageIndex: function (TableName, Index) {
        if (Index != undefined && Index != null) {
            if (Index == true) {
                this.List[TableName].PageIndex = 1;
            }
            else { this.List[TableName].PageIndex = Index; }
        }
        try {
            this.List[TableName].LoadFunction(this.List[TableName].PageIndex, Table.List[TableName].PageSize, true);
            this.List[TableName].LastPageIndex = this.List[TableName].PageIndex;
        }
        catch (e) { }
    }
}