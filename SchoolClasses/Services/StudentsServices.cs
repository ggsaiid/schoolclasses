﻿using Microsoft.Extensions.Logging;
using SchoolClasses.Contracts;
using SchoolClasses.Controllers;
using SchoolClasses.Data;
using SchoolClasses.LogProvider;
using SchoolClasses.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SchoolClasses.Sevices
{
    public class StudentsServices : IStudentsService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<StudentsController> _logger;

        public StudentsServices(ApplicationDbContext context, ILogger<StudentsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public List<Students> GetStudentsList(int classId)
        {
            _logger.LogInformation((int)LoggingEvents.LIST_ITEMS, "Retrieve Students list.");
            return _context.Students.Where(x => x.ClassId == classId).ToList();
        }

        public int Create(Students studentInfo)
        {
            try
            {
                var student = _context.Students.Where(x => x.StudentName == studentInfo.StudentName).SingleOrDefault();
                if (student != null && student.StudentName == studentInfo.StudentName)
                {
                    _logger.LogWarning((int)LoggingEvents.INSERT_ITEM, "Student Information is already Exist.");
                    return -10;
                }
                _context.Add(studentInfo);
                int res = _context.SaveChanges();
                if (res > 0)
                    _logger.LogInformation((int)LoggingEvents.INSERT_ITEM, "New Student Added.");
                else
                    _logger.LogWarning((int)LoggingEvents.INSERT_ITEM, "Adding New Student Failed.");
                return res;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.INSERT_ITEM, E.Message);
                return -2;
            }
        }

        public int Update(Students studentInfo)
        {
            try
            {
                var student = _context.Students.Where(x =>
                x.StudentName == studentInfo.StudentName).SingleOrDefault();

                if (student != null)
                {
                    if (student.StudentId != studentInfo.StudentId)
                    {
                        if (student.StudentName == studentInfo.StudentName)
                        {
                            _logger.LogWarning((int)LoggingEvents.INSERT_ITEM, "Student Information is already Exist.");
                            return -10;
                        }
                    }
                    student.StudentName = studentInfo.StudentName;
                    student.GPA = studentInfo.GPA;
                    student.Age = studentInfo.Age;
                    _context.Update(student);
                }
                else
                    _context.Update(studentInfo);

                int res = _context.SaveChanges();
                if (res > 0)
                    _logger.LogInformation((int)LoggingEvents.UPDATE_ITEM, "the Student Information Updated.");
                else
                    _logger.LogWarning((int)LoggingEvents.UPDATE_ITEM, "the Student Updating Failed.");
                return res;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.UPDATE_ITEM, E.Message);
                return -2;
            }
        }

        public int Delete(int studentId)
        {
            try
            {
                var student = Select(studentId);
                _context.Students.Remove(student);
                int res = _context.SaveChanges();
                if (res > 0)
                    _logger.LogInformation((int)LoggingEvents.DELETE_ITEM, "the Student Deleted.");
                else
                    _logger.LogWarning((int)LoggingEvents.DELETE_ITEM, "Student Deleting Failed.");
                return res;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.DELETE_ITEM, E.Message);
                return -2;
            }
        }

        public Students Select(int studentId)
        {
            try
            {
                var student = _context.Students.Find(studentId);
                if (student == null)
                {
                    _logger.LogInformation((int)LoggingEvents.GET_ITEM, "the Student doesnt exist.");
                    return null;
                }
                return student;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.GET_ITEM, E.Message);
                return null;
            }
        }

        public bool Exists(int studentId)
        {
            return _context.Students.Any(e => e.StudentId == studentId);
        }

        public void LogWarning(LoggingEvents loggingEvents, string message)
        {
            _logger.LogWarning((int)loggingEvents, message);
        }
    }
}
