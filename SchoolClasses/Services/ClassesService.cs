﻿using Microsoft.Extensions.Logging;
using SchoolClasses.Contracts;
using SchoolClasses.Controllers;
using SchoolClasses.Data;
using SchoolClasses.LogProvider;
using SchoolClasses.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SchoolClasses.Sevices
{
    public class ClassesService : IClassesService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<HomeController> _logger;

        public ClassesService(ApplicationDbContext context, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public List<Classes> GetClassesList()
        {
            _logger.LogInformation((int)LoggingEvents.LIST_ITEMS, "Retrieve Classes list.");
            return _context.Classes.ToList();
        }
        
        public int Create(Classes classInfo)
        {
            try
            {
                _context.Add(classInfo);
                int res = _context.SaveChanges();
                if (res > 0)
                    _logger.LogInformation((int)LoggingEvents.INSERT_ITEM, "New Class Added.");
                else
                    _logger.LogWarning((int)LoggingEvents.INSERT_ITEM, "Adding New Class Failed.");
                return res;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.INSERT_ITEM, E.Message);
                return -2;
            }
        }

        public int Update(Classes classInfo)
        {
            try
            {
                _context.Update(classInfo);
                int res = _context.SaveChanges();
                if (res > 0)
                    _logger.LogInformation((int)LoggingEvents.UPDATE_ITEM, "the Class Information Updated.");
                else
                    _logger.LogWarning((int)LoggingEvents.UPDATE_ITEM, "the Class Updating Failed.");
                return res;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.UPDATE_ITEM, E.Message);
                return -2;
            }
        }

        public int Delete(int classId)
        {
            try
            {
                var classes = Select(classId);
                _context.Classes.Remove(classes);
                int res = _context.SaveChanges();
                if (res > 0)
                    _logger.LogInformation((int)LoggingEvents.DELETE_ITEM, "the Class Deleted.");
                else
                    _logger.LogWarning((int)LoggingEvents.DELETE_ITEM, "Class Deleting Failed.");
                return res;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.DELETE_ITEM, E.Message);
                return -2;
            }
        }

        public Classes Select(int classId)
        {
            try
            {
                var classes = _context.Classes.Find(classId);
                if (classes == null)
                {
                    _logger.LogInformation((int)LoggingEvents.GET_ITEM, "the Class doesnt exist.");
                    return null;
                }
                return classes;
            }
            catch (Exception E)
            {
                _logger.LogError((int)LoggingEvents.GET_ITEM, E.Message);
                return null;
            }
        }

        public bool Exists(int classId)
        {
            return _context.Classes.Any(e => e.ClassId == classId);
        }

        public void LogWarning(LoggingEvents loggingEvents, string message)
        {
            _logger.LogWarning((int)loggingEvents, message);
        }
    }
}
