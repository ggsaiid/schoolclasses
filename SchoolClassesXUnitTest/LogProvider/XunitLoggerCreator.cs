﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolClasses.LogProvider;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit.Abstractions;

namespace SchoolClassesXUnitTest.LogProvider
{
    class XunitLoggerCreator
    {
        public static ILogger<T> XunitLogger<T>(ITestOutputHelper testOutputHelper, string connectionString) where T : Controller
        {
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new XunitLoggerProvider(testOutputHelper));
            loggerFactory.AddContext(LogLevel.Information, connectionString);

            return loggerFactory.CreateLogger<T>();
        }
    }
}
