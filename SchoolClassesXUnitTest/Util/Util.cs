﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolClasses.Data;
using Xunit;
using Xunit.Abstractions;

namespace SchoolClassesXUnitTest.Util
{
    public static class Util
    {
        public const string connectionString = "Server=(local);Database=SchoolClasses;Trusted_Connection=True;MultipleActiveResultSets=true";

        public static ApplicationDbContext CreateDbContext(string connectionString)
        {
            return new ApplicationDbContext(new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseSqlServer(connectionString).Options);
        }
    }
}
public static class IActionResultExtension
{
    public static IActionResult CheckResult(this IActionResult actionResult, ITestOutputHelper _testOutput)
    {
        if (actionResult is JsonResult)
        {
            JsonResult jsonResult = (JsonResult)actionResult;
            _testOutput.WriteLine(jsonResult.Value.ToString());
            if ((jsonResult.Value.ToString().Contains("actionstate = warning"))
                || (jsonResult.Value.ToString().Contains("actionstate = danger")))
            {
                Assert.True(false);
                return actionResult;
            }
            Assert.True(true);
            return actionResult;
        }
        Assert.True(false);
        return actionResult;
    }
    public static IActionResult CheckResult(this IActionResult actionResult)
    {
        if (actionResult is JsonResult)
        {
            JsonResult jsonResult = (JsonResult)actionResult;
            if ((jsonResult.Value.ToString().Contains("actionstate = warning"))
                || (jsonResult.Value.ToString().Contains("actionstate = danger")))
            {
                Assert.True(false);
                return actionResult;
            }
            Assert.True(true);
            return actionResult;
        }
        Assert.True(false);
        return actionResult;
    }
}
