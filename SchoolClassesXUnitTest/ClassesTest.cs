using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolClasses.Controllers;
using SchoolClasses.Models;
using SchoolClasses.Sevices;
using SchoolClassesXUnitTest.LogProvider;
using Xunit;
using Xunit.Abstractions;

namespace SchoolClassesXUnitTest
{
    public class ClassesTest
    {
        private const int ClassId = 33;

        private readonly Classes NewInfo = new Classes
        {
            ClassName = "Math",
            Location = "England",
            TeacherName = "David"
        };
        private readonly Classes UpdateInfo = new Classes
        {
            ClassId = ClassId,
            ClassName = "Computer Science",
            Location = "France",
            TeacherName = "Ana"
        };

        private readonly HomeController _controller;
        private readonly ILogger<HomeController> _logger;
        private readonly ITestOutputHelper _testOutput;

        public ClassesTest(ITestOutputHelper testOutputHelper)
        {
            _testOutput = testOutputHelper;
            _logger = XunitLoggerCreator.XunitLogger<HomeController>(testOutputHelper, Util.Util.connectionString);            
            _controller = CreateClassesController(_logger);
        }

        [Fact]
        public void Create()
        {
            _controller.Create(NewInfo).CheckResult(_testOutput);
        }
        [Fact]
        public void List()
        {
            _controller.List().CheckResult();
        }
        [Fact]
        public void Select()
        {
            _controller.Select(ClassId).CheckResult();
        }
        [Fact]
        public void Update()
        {
            _controller.Update(UpdateInfo).CheckResult();
        }
        [Fact]
        public void Delete()
        {
            _controller.Delete(ClassId).CheckResult();
        }

        private HomeController CreateClassesController(ILogger<HomeController> logger)
        {
            ClassesService service = new ClassesService(Util.Util.CreateDbContext(Util.Util.connectionString), logger);
            return new HomeController(service);
        }
    }
}