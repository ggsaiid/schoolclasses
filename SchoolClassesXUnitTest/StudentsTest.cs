using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SchoolClasses.Controllers;
using SchoolClasses.Models;
using SchoolClasses.Sevices;
using SchoolClassesXUnitTest.LogProvider;
using Xunit;
using Xunit.Abstractions;

namespace SchoolClassesXUnitTest
{
    public class StudentsTest
    {
        private const int ClassId = 34;
        private const int StudentId = 28;

        private readonly Students NewInfo = new Students
        {
            ClassId = ClassId,
            Age = "20",
            GPA = "20",
            StudentName = "Sarah"
        };
        private readonly Students UpdateInfo = new Students
        {
            ClassId = ClassId,
            StudentId = StudentId,
            Age = "22",
            GPA = "32",
            StudentName = "sarah conners"
        };

        private readonly StudentsController _controller;
        private readonly ILogger<StudentsController> _logger;
        private readonly ITestOutputHelper _testOutput;

        public StudentsTest(ITestOutputHelper testOutputHelper)
        {
            _testOutput = testOutputHelper;
            _logger = XunitLoggerCreator.XunitLogger<StudentsController>(testOutputHelper, Util.Util.connectionString);
            _controller = CreateStudentsController(_logger);
        }

        [Fact]
        public void Create()
        {
            _controller.Create(NewInfo).CheckResult();
        }

        [Fact]
        public void List()
        {
            _controller.List(ClassId).CheckResult();
        }

        [Fact]
        public void Select()
        {
            _controller.Select(StudentId).CheckResult();
        }

        [Fact]
        public void Update()
        {
            _controller.Update(UpdateInfo).CheckResult(_testOutput);
        }

        [Fact]
        public void Delete()
        {
            _controller.Delete(StudentId).CheckResult();
        }

        private StudentsController CreateStudentsController(ILogger<StudentsController> logger)
        {
            StudentsServices service = new StudentsServices(Util.Util.CreateDbContext(Util.Util.connectionString), logger);
            return new StudentsController(service);
        }
    }
}